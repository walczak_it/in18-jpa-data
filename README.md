## Requiorments ##

You must hava JDK 11 or higher and Postgres 9.6 or higher installed on your system.

You must create a super user in18jpa with password 123456in Postgres and an owned by him database
called in18jpa.

## Running ##

### Concept A ###

Run application:

./gradlew bootRunA

Populate datadatabase:

./gradlew gatlingRun-in18jpa.ConceptAPopulateSimulation

Run full simulation:

./gradlew gatlingRun-in18jpa.ConceptAFullSimulation

### Concept B ###

Run application:

./gradlew bootRunB

Populate datadatabase:

./gradlew gatlingRun-in18jpa.ConceptBPopulateSimulation

Run full simulation:

./gradlew gatlingRun-in18jpa.ConceptBFullSimulation
