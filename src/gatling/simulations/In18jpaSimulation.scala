//netsh int ipv4 set dynamic tcp start=1025 num=57975

package in18jpa

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.http.protocol.HttpProtocolBuilder
import scala.concurrent.duration._
import scala.util._

abstract class In18jpaSimulation extends Simulation {

  def createHttpProtocol(): HttpProtocolBuilder  = {
    http
      .baseUrl("http://localhost:8080")
      .acceptHeader("application/json")
      .contentTypeHeader("application/json")
      .doNotTrackHeader("1")
      .acceptLanguageHeader("pl-PL")
      .acceptEncodingHeader("gzip, deflate")
      .shareConnections
  }
    
  def createFeeder() = {
    Iterator.continually(
      Map(
        "code" -> Random.alphanumeric
          .take(Random.nextInt(10)).mkString(""),
        "name" -> Random.alphanumeric
          .take(Random.nextInt(20)).mkString(""),
        "description" -> Random.alphanumeric
          .take(Random.nextInt(100)).mkString(""),
        "price" -> Random.nextDouble(),
        "nameTranslation" -> Random.alphanumeric
          .take(Random.nextInt(20)).mkString(""),
        "descriptionTranslation" -> Random.alphanumeric
          .take(Random.nextInt(100)).mkString(""),
        "namePart" -> Random.alphanumeric
          .take(Random.nextInt(2) + 2).mkString("")
      )
    )
  }
}

