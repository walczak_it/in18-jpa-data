package in18jpa

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._
import scala.util._

class ConceptAFullSimulation extends In18jpaSimulation {

  val scn = scenario("Concept A - Full simulation")
    .repeat(10000)(
        feed(createFeeder())
        .exec(
          http("add-product")
            .post("/products")
            .body(StringBody(
              """
                 | {
                 |    "code": "${code}",
                 |    "name": "${name}",
                 |    "description": "${description}",
                 |    "price": ${price}
                 | }
              """.stripMargin))
            .check(jsonPath("$.id").saveAs("id"))
        )
        .exec(
          http("add-translation")
            .put("/products/${id}/translations/pl")
            .body(StringBody(
              """
                 | {
                 |    "name": "${nameTranslation}",
                 |    "description": "${descriptionTranslation}"
                 | }
              """.stripMargin))
        )
        .exec(
          http("get-translated")
            .get("/products/${id}")
        )
        .exec(
          http("find-by-name-part")
            .get("/products?name=${namePart}")
        )
    )

  setUp(
      scn.inject(
          atOnceUsers(1),
          nothingFor(5 seconds),
          rampUsers(5) during (20 seconds)
        )
      .protocols(createHttpProtocol())
  )
}
