CREATE INDEX product_details_name_idx ON Product
USING GIN (lower(details_name_value) gin_trgm_ops);

CREATE INDEX product_translated_name_idx ON Product
USING GIN (lower(details_name_translationsbylanguage -> 'pl') gin_trgm_ops);
