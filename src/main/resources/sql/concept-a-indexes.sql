CREATE INDEX product_details_name_idx ON product 
USING GIN (lower(details_name) gin_trgm_ops);

CREATE INDEX product_translated_name_idx ON product_translateddetailsbylanguage
USING GIN (lower(translateddetailsbylanguage_value_name) gin_trgm_ops);
