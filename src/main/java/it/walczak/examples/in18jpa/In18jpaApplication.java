package it.walczak.examples.in18jpa;

import it.walczak.examples.in18jpa.concepts.a.AConfiguration;
import it.walczak.examples.in18jpa.concepts.b.BConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class In18jpaApplication {

    public static void main(String[] args) {
        String concept = System.getProperty("CONCEPT");
        if (concept == null) {
            System.err.println("Missing CONCEPT env variable");
            System.exit(-1);
        } else if (concept.equalsIgnoreCase("A")) {
            new SpringApplicationBuilder(AConfiguration.class)
                .profiles("concept-a")
                .run(args);
        } else if (concept.equalsIgnoreCase("B")) {
            new SpringApplicationBuilder(BConfiguration.class)
                .profiles("concept-b")
                .run(args);
        } else {
            System.err.println("Unrecognized CONCEPT: " + concept);
            System.exit(-1);
        }
    }
}
