package it.walczak.examples.in18jpa.concepts.a.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import org.springframework.context.i18n.LocaleContextHolder;

@Entity
public class Product {

    @Id
    private UUID id = UUID.randomUUID();
    
    @Embedded
    private ProductDetails details;
    
    @ElementCollection(fetch = FetchType.LAZY)
    private Map<String, TranslatableProductDetails> translatedDetailsByLanguage = new HashMap<>();

    public Product() { }
    
    public Product(ProductDetails details) {
        this.details = details;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ProductDetails getDetails() {
        return details;
    }

    public void setDetails(ProductDetails details) {
        this.details = details;
    }
    
    public TranslatableProductDetails getTranslatedDetails() {
        String language = LocaleContextHolder.getLocale().getLanguage();
        return getTranslatedDetailsByLocale().get(language);
    }

    @JsonIgnore
    public Map<String, TranslatableProductDetails> getTranslatedDetailsByLocale() {
        return translatedDetailsByLanguage;
    }

    public void setTranslatedDetailsByLocale(
        Map<String, TranslatableProductDetails> translatedDetailsByLocale
    ) {
        this.translatedDetailsByLanguage = translatedDetailsByLocale;
    }
}
