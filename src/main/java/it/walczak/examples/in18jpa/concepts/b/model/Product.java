package it.walczak.examples.in18jpa.concepts.b.model;

import java.util.UUID;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Product {

    @Id
    private UUID id = UUID.randomUUID();
    
    @Embedded
    private ProductDetails details;

    public Product() { }
    
    public Product(ProductDetails details) {
        this.details = details;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public ProductDetails getDetails() {
        return details;
    }

    public void setDetails(ProductDetails details) {
        this.details = details;
    }
}
