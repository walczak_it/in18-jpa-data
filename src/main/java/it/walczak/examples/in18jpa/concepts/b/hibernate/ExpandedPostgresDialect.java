package it.walczak.examples.in18jpa.concepts.b.hibernate;

import org.hibernate.dialect.PostgreSQL95Dialect;

public class ExpandedPostgresDialect extends PostgreSQL95Dialect {

	public ExpandedPostgresDialect() {
		super();
		registerFunction("hstoreValue", new HStoreValueFunction());
	}
}
