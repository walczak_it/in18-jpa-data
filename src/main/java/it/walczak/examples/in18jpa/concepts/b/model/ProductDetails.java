package it.walczak.examples.in18jpa.concepts.b.model;

import java.math.BigDecimal;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
public class ProductDetails {
    
    private String code;
    
    @Embedded
    private TranslatableField name;
    
    @Embedded
    private TranslatableField description;
    
    private BigDecimal price;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public TranslatableField getName() {
        return name;
    }

    public void setName(TranslatableField name) {
        this.name = name;
    }

    public TranslatableField getDescription() {
        return description;
    }

    public void setDescription(TranslatableField description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
