package it.walczak.examples.in18jpa.concepts.b.model;

import java.util.HashMap;
import java.util.Map;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.hibernate.annotations.Type;
import org.springframework.context.i18n.LocaleContextHolder;

@Embeddable
public class TranslatableField {
    
    private String value;
    
    @Type(type = "it.walczak.examples.in18jpa.concepts.b.hibernate.HStoreType")
    @Column(columnDefinition = "hstore")
    private Map<String, String> translationsByLanguage = new HashMap<>();

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
    
    public String getTranslation() {
        String language = LocaleContextHolder.getLocale().getLanguage();
        return getTranslationByLanguage().get(language);
    }

    public Map<String, String> getTranslationByLanguage() {
        return translationsByLanguage;
    }

    public void setTranslationByLanguage(Map<String, String> translationByLanguage) {
        this.translationsByLanguage = translationByLanguage;
    }
}
