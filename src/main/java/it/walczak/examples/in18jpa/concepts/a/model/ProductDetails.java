package it.walczak.examples.in18jpa.concepts.a.model;

import java.math.BigDecimal;
import javax.persistence.Embeddable;

@Embeddable
public class ProductDetails extends TranslatableProductDetails {
    
    private String code;
    private BigDecimal price;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
